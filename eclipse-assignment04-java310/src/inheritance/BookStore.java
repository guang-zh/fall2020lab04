/**
 * Guang Kun Zhang
 * 1942372
 * Lab4 Java310 

 * @author guang
 * {@link https://gitlab.com/guang-zh/fall2020lab04.git}
 */

package inheritance;

public class BookStore {

	public static void main(String[] args) {
		// Create Book[] of size 5; use both Book and ElectronicBook classes
		Book[] bookArr = new Book[5];
		bookArr[0] = new Book("Java is Blue", "Java310");
		bookArr[1] = new ElectronicBook("JavaScript is Red", "WebDev", 60);
		bookArr[2] = new Book("Bash is Green", "Admins");
		bookArr[3] = new ElectronicBook("Python is Hot", "Market", 100);
		bookArr[4] = new ElectronicBook("SQL is Yellow", "DataMinions", 80);
		
		for (Book b : bookArr) {
			System.out.println(b.toString());
		}
	}
}


