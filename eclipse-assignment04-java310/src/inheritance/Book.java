/**
 * Guang Kun Zhang
 * 1942372
 * Lab4 Java310 

 * @author guang
 * {@link https://gitlab.com/guang-zh/fall2020lab04.git}
 */

package inheritance;

public class Book {
	
	protected String title;
	private String author;
	
	public Book(String title, String author) {
		// Constructor with 2 fields as input
		this.title = title;
		this.author = author;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public String getAuthor() {
		return this.author;
	}
	
	public String toString() {
		return ("Book Title: "+title+
				"; Book Author: "+author);
	}

	

}
