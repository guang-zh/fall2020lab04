/**
 * Guang Kun Zhang
 * 1942372
 * Lab4 Java310 

 * @author guang
 * {@link https://gitlab.com/guang-zh/fall2020lab04.git}
 */

package inheritance;
public class ElectronicBook extends Book {
	
	private int numberBytes;

	public ElectronicBook(String title, String author, int numberBytes) {
		// Use super() to obtain from base constructor; 
		// Test with *set* for private field author
		super(title, author);
		// this.title=title;
		// this.author=author;
		this.numberBytes=numberBytes;
	}
//	public void setAuthor(String author) {
//		this.author = author;
//	}
	
	public String toString() {
		String fromBase = super.toString();
		return (fromBase +
				"; Number of Bytes: "+ this.numberBytes);
	}
}
