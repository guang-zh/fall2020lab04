/**
 * Guang Kun Zhang
 * 1942372
 * Lab4 Java310 

 * @author guang
 * {@link https://gitlab.com/guang-zh/fall2020lab04.git}
 */
package geometry;

public class Square extends Rectangle {

	public Square(double sideLength) {
		// inherits from Rectangle class
		super(sideLength, sideLength);
	}
}
