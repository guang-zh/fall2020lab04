/**
 * Guang Kun Zhang
 * 1942372
 * Lab4 Java310 

 * @author guang
 * {@link https://gitlab.com/guang-zh/fall2020lab04.git}
 */
package geometry;

public class Circle implements Shape {
	private double radius;
	public Circle(double radius) {
		// Input double radius and set value of radius
		this.radius = radius;
	}
	public double getRadius() {
		return this.radius;
	}
	public double getArea() {
		return (Math.PI * Math.pow(this.radius, 2));
	}
	public double getPerimeter() {
		return (2 * Math.PI * this.radius);
	}
}
