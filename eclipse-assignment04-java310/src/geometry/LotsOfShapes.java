/**
 * Guang Kun Zhang
 * 1942372
 * Lab4 Java310 

 * @author guang
 * {@link https://gitlab.com/guang-zh/fall2020lab04.git}
 */
package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapeArr = new Shape[5];
//		shapeArr[0] = new Shape();
		
		shapeArr[0] = new Rectangle(80, 50);
		shapeArr[1] = new Rectangle(100, 80);
		shapeArr[2] = new Circle(8);
		shapeArr[3] = new Circle(12);
		shapeArr[4] = new Square(21);
		
		for (Shape s: shapeArr) {
			System.out.println(s.getClass().getSimpleName());
			System.out.println("Shape Area: "+s.getArea()+
					"; Shape Perimeter: "+s.getPerimeter());
		}
	}
}

