/**
 * Guang Kun Zhang
 * 1942372
 * Lab4 Java310 

 * @author guang
 * {@link https://gitlab.com/guang-zh/fall2020lab04.git}
 */
package geometry;

public class Rectangle implements Shape {
	private double length;
	private double width;

	public Rectangle(double length, double width) {
		this.length = length;
		this.width = width;
	}
	
	public double getLength() {
		return this.length;
	}
	public double getWidth() {
		return this.width;
	}
	public double getArea() {
		return (this.length * this.width);
	}
	public double getPerimeter() {
		return (2 * (this.length + this.width));
	}
}
